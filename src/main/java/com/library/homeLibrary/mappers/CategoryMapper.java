package com.library.homeLibrary.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.domain.Category;
import com.library.homeLibrary.dto.CategoryDto;
import com.library.homeLibrary.repository.BookRepository;

@Component
public class CategoryMapper implements BaseMapper<Category, CategoryDto> {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public CategoryDto fromDomainToDto(Category category) {
		List<String> books = new ArrayList<>();
		for (Book bookName : category.getBooks()) {
			books.add(bookName.getBookName());
		}
		return new CategoryDto(category.getId(), category.getCategoryName(), books);
	}

	@Override
	public Category fromDtoToDomain(CategoryDto categoryDto) {
		Category category;
		List<Book> books = new ArrayList<>();
		if (categoryDto.getBooks() == null) {
			category = new Category(categoryDto.getId(), categoryDto.getCategoryName(), books);
		} else {
			for (String bookName : categoryDto.getBooks()) {
				books.addAll(bookRepository.findByBookName(bookName));
			}
			category = new Category(categoryDto.getId(), categoryDto.getCategoryName(), books);
		}
		return category;
	}

	@Override
	public List<Category> fromDtoListToDomainList(List<CategoryDto> categoriesDto) {
		List<Category> categories = new ArrayList<>();
		for (CategoryDto categoryDto : categoriesDto) {
			categories.add(fromDtoToDomain(categoryDto));
		}
		return categories;
	}

	@Override
	public List<CategoryDto> fromDomainListToDtoList(List<Category> categories) {
		List<CategoryDto> categoriesDto = new ArrayList<>();
		for (Category category : categories) {
			categoriesDto.add(fromDomainToDto(category));
		}
		return categoriesDto;
	}

}
