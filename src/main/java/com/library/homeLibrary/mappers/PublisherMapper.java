package com.library.homeLibrary.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.domain.Publisher;
import com.library.homeLibrary.dto.PublisherDto;
import com.library.homeLibrary.repository.BookRepository;

@Component
public class PublisherMapper implements BaseMapper<Publisher, PublisherDto> {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public PublisherDto fromDomainToDto(Publisher publisher) {
		List<String> books = new ArrayList<>();
		for (Book bookName : publisher.getBooks()) {
			books.add(bookName.getBookName());
		}
		return new PublisherDto(publisher.getId(), publisher.getPublisherName(), books);
	}

	@Override
	public Publisher fromDtoToDomain(PublisherDto publisherDto) {
		Publisher publisher;
		List<Book> books = new ArrayList<>();

		if (publisherDto.getBooks() == null) {
			publisher = new Publisher(publisherDto.getId(), publisherDto.getPublisherName(), books);
		} else {
			for (String bookName : publisherDto.getBooks()) {
				books.addAll(bookRepository.findByBookName(bookName));
			}
			publisher = new Publisher(publisherDto.getId(), publisherDto.getPublisherName(), books);
		}

		return publisher;
	}

	@Override
	public List<Publisher> fromDtoListToDomainList(List<PublisherDto> publishersDto) {
		List<Publisher> publishers = new ArrayList<>();
		for (PublisherDto publisherDto : publishersDto) {
			publishers.add(fromDtoToDomain(publisherDto));
		}
		return publishers;
	}

	@Override
	public List<PublisherDto> fromDomainListToDtoList(List<Publisher> publishers) {
		List<PublisherDto> publishersDto = new ArrayList<>();
		for (Publisher publisher : publishers) {
			publishersDto.add(fromDomainToDto(publisher));
		}
		return publishersDto;
	}

}
