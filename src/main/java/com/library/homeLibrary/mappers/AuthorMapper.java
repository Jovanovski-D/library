package com.library.homeLibrary.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.library.homeLibrary.domain.Author;
import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.dto.AuthorDto;
import com.library.homeLibrary.repository.BookRepository;

@Component
public class AuthorMapper implements BaseMapper<Author, AuthorDto> {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public AuthorDto fromDomainToDto(Author author) {
		List<String> books = new ArrayList<>();
		for (Book bookName : author.getBooks()) {
			books.add(bookName.getBookName());
		}
		return new AuthorDto(author.getId(), author.getFirstName(), books);

	}

	@Override
	public Author fromDtoToDomain(AuthorDto authorDto) {
		Author author;
		List<Book> books = new ArrayList<>();

		if (authorDto.getBooks() == null) {
			author = new Author(authorDto.getId(), authorDto.getFirstName(), books);
		} else {
			for (String bookName : authorDto.getBooks()) {
				books.addAll(bookRepository.findByBookName(bookName));
			}
			author = new Author(authorDto.getId(), authorDto.getFirstName(), books);
		}
		return author;
	}

	@Override
	public List<Author> fromDtoListToDomainList(List<AuthorDto> authorsDto) {
		List<Author> authors = new ArrayList<>();
		for (AuthorDto authorDto : authorsDto) {
			authors.add(fromDtoToDomain(authorDto));
		}
		return authors;
	}

	@Override
	public List<AuthorDto> fromDomainListToDtoList(List<Author> authors) {
		List<AuthorDto> authorsDto = new ArrayList<>();
		for (Author author : authors) {
			authorsDto.add(fromDomainToDto(author));
		}
		return authorsDto;
	}

}
