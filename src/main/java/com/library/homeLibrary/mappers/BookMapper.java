package com.library.homeLibrary.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.library.homeLibrary.domain.Author;
import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.domain.Category;
import com.library.homeLibrary.domain.Format;
import com.library.homeLibrary.domain.Publisher;
import com.library.homeLibrary.dto.AuthorDto;
import com.library.homeLibrary.dto.BookDto;
import com.library.homeLibrary.dto.CategoryDto;
import com.library.homeLibrary.dto.FormatDto;
import com.library.homeLibrary.dto.PublisherDto;

@Component
public class BookMapper implements BaseMapper<Book, BookDto> {

	@Autowired
	private PublisherMapper publisherMapper;
	@Autowired
	private FormatMapper formatMapper;
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private AuthorMapper authorMapper;

	@Override
	public BookDto fromDomainToDto(Book book) {

		PublisherDto publisherDto = publisherMapper.fromDomainToDto(book.getPublisher());

		List<FormatDto> formatsDto = formatMapper.fromDomainListToDtoList(book.getFormats());

		List<AuthorDto> authorsDto = authorMapper.fromDomainListToDtoList(book.getAuthors());
		
		List<CategoryDto> categoriesDto = categoryMapper.fromDomainListToDtoList(book.getCategories());

		BookDto bookDto = new BookDto(book.getId(), book.getBookName(), book.getIssueYear(), book.getEdition(),
				publisherDto, formatsDto, authorsDto, categoriesDto);

		return bookDto;
	}

	@Override
	public Book fromDtoToDomain(BookDto bookDto) {

		Publisher publisher = publisherMapper.fromDtoToDomain(bookDto.getPublisher());

		List<Format> formats = formatMapper.fromDtoListToDomainList(bookDto.getFormats());

		List<Author> authors = authorMapper.fromDtoListToDomainList(bookDto.getAuthors());

		List<Category> categories = categoryMapper.fromDtoListToDomainList(bookDto.getCategories());

		Book book = new Book(bookDto.getId(), bookDto.getBookName(), bookDto.getIssueYear(), bookDto.getEdition(),
				publisher, formats, authors, categories);

		return book;
	}

	@Override
	public List<Book> fromDtoListToDomainList(List<BookDto> bookDtos) {
		List<Book> books = new ArrayList<>();
		for (BookDto bookDto : bookDtos) {
			books.add(fromDtoToDomain(bookDto));
		}
		return books;
	}

	@Override
	public List<BookDto> fromDomainListToDtoList(List<Book> books) {
		List<BookDto> booksDto = new ArrayList<>();
		for (Book book : books) {
			booksDto.add(fromDomainToDto(book));
		}
		return booksDto;
	}

}
