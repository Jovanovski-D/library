package com.library.homeLibrary.mappers;

import java.util.List;

public interface BaseMapper<Domain, Dto> {

	public Dto fromDomainToDto(Domain domain);

	public Domain fromDtoToDomain(Dto dto);

	public List<Domain> fromDtoListToDomainList(List<Dto> dto);

	public List<Dto> fromDomainListToDtoList(List<Domain> domain);
}
