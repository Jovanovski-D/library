package com.library.homeLibrary.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.domain.Format;
import com.library.homeLibrary.dto.FormatDto;
import com.library.homeLibrary.repository.BookRepository;

@Component
public class FormatMapper implements BaseMapper<Format, FormatDto> {

	@Autowired
	private BookRepository bookRepository;
	
	@Override
	public FormatDto fromDomainToDto(Format format) {
		List<String> books = new ArrayList<>();
		for (Book bookName : format.getBooks()) {
			books.add(bookName.getBookName());
		}
		return new FormatDto(format.getId(), format.getFormatName(), books);
	}

	@Override
	public Format fromDtoToDomain(FormatDto formatDto) {
		Format format;
		List<Book> books = new ArrayList<>();
		if (formatDto.getBooks() == null) {
			format = new Format(formatDto.getId(), formatDto.getFormatName(), books);
		} else {
			for (String bookName : formatDto.getBooks()) {
				books.addAll(bookRepository.findByBookName(bookName));
			}
			format = new Format(formatDto.getId(), formatDto.getFormatName(), books);
		}
		return format;
	}

	@Override
	public List<Format> fromDtoListToDomainList(List<FormatDto> formatsDto) {
		List<Format> formats = new ArrayList<>();
		for (FormatDto formatDto : formatsDto) {
			formats.add(fromDtoToDomain(formatDto));
		}
		return formats;
	}

	@Override
	public List<FormatDto> fromDomainListToDtoList(List<Format> formats) {
		List<FormatDto> formatsDto = new ArrayList<>();
		for (Format format : formats) {
			formatsDto.add(fromDomainToDto(format));
		}
		return formatsDto;
	}
}
