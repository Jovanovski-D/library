package com.library.homeLibrary.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class AuthorDto {

	private Integer id;

	private String firstName;

	private List<String> books;
}
