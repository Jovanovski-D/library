package com.library.homeLibrary.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CategoryDto {

	private Integer id;

	private String categoryName;

	private List<String> books;
}
