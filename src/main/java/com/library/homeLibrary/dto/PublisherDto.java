package com.library.homeLibrary.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class PublisherDto {

	private Integer id;

	private String publisherName;

	private List<String> books;
}
