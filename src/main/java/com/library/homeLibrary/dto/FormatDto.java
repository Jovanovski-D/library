package com.library.homeLibrary.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class FormatDto {

	private Integer id;

	private String formatName;

	private List<String> books;

}
