package com.library.homeLibrary.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class BookDto {

	private Integer id;

	private String bookName;

	private Integer issueYear;

	private Integer edition;

	private PublisherDto publisher;

	private List<FormatDto> formats;

	private List<AuthorDto> authors;

	private List<CategoryDto> categories;

}
