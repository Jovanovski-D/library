package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.homeLibrary.domain.Format;
import com.library.homeLibrary.dto.FormatDto;
import com.library.homeLibrary.mappers.FormatMapper;
import com.library.homeLibrary.repository.FormatRepository;

@Service
public class FormatServiceImpl implements FormatService {

	@Autowired
	private FormatRepository formatRepository;
	@Autowired
	private FormatMapper formatMapper;

	@Override
	public List<FormatDto> findAll() {
		List<Format> formats = formatRepository.findAll();
		List<FormatDto> formatsDto = formatMapper.fromDomainListToDtoList(formats);
		return formatsDto;
	}

	@Override
	public FormatDto findById(Integer id) {
		Format format = formatRepository.findById(id).orElseThrow(RuntimeException::new);
		FormatDto formatDto = formatMapper.fromDomainToDto(format);
		return formatDto;
	}

	@Override
	public void saveFormat(FormatDto formatDto) {
		Format format = formatMapper.fromDtoToDomain(formatDto);
		formatRepository.save(format);
	}

	@Override
	public void deleteById(Integer id) {
		formatRepository.deleteById(id);
	}

	@Override
	public void update(Integer id, FormatDto formatDto) {
		Format format = formatMapper.fromDtoToDomain(formatDto);
		formatRepository.save(format);
	}

}
