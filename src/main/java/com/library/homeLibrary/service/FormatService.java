package com.library.homeLibrary.service;

import java.util.List;

import com.library.homeLibrary.dto.FormatDto;

public interface FormatService {

	public FormatDto findById(Integer id);

	public void saveFormat(FormatDto formatDto);

	public List<FormatDto> findAll();

	public void deleteById(Integer id);

	public void update(Integer id, FormatDto formatDto);

}
