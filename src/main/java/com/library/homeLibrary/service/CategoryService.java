package com.library.homeLibrary.service;

import java.util.List;

import com.library.homeLibrary.dto.CategoryDto;

public interface CategoryService {

	public CategoryDto findById(Integer id);

	public void saveCategory(CategoryDto categoryDto);

	public List<CategoryDto> findAll();

	public void deleteById(Integer id);

	public void update(Integer id, CategoryDto categoryDto);
}
