package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.homeLibrary.domain.Author;
import com.library.homeLibrary.dto.AuthorDto;
import com.library.homeLibrary.mappers.AuthorMapper;
import com.library.homeLibrary.repository.AuthorRepository;

@Service
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorRepository authorRepository;
	@Autowired
	private AuthorMapper authorMapper;

	@Override
	public List<AuthorDto> findAll() {
		List<Author> authors = authorRepository.findAll();
		List<AuthorDto> authorsDto = authorMapper.fromDomainListToDtoList(authors);
		return authorsDto;
	}

	@Override
	public AuthorDto findById(Integer id) {
		Author author = authorRepository.findById(id).orElseThrow(RuntimeException::new);
		AuthorDto authorDto = authorMapper.fromDomainToDto(author);
		return authorDto;
	}

	@Override
	public void saveAuthor(AuthorDto authorDto) {
		Author author = authorMapper.fromDtoToDomain(authorDto);
		authorRepository.save(author);
	}

	@Override
	public void deleteById(Integer id) {
		authorRepository.deleteById(id);
	}

	@Override
	public void update(Integer id, AuthorDto authorDto) {
		Author author = authorMapper.fromDtoToDomain(authorDto);
		authorRepository.save(author);
	}

}
