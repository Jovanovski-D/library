package com.library.homeLibrary.service;

import java.util.List;

import com.library.homeLibrary.dto.PublisherDto;

public interface PublisherService {

	public PublisherDto findById(Integer id);

	public void savePublisher(PublisherDto publisherDto);

	public List<PublisherDto> findAll();

	public void deleteById(Integer id);

	public void update(Integer id, PublisherDto publisherDto);

}
