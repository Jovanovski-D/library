package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.homeLibrary.domain.Book;
import com.library.homeLibrary.dto.BookDto;
import com.library.homeLibrary.mappers.BookMapper;
import com.library.homeLibrary.repository.BookRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private BookMapper bookMapper;

	@Override
	public List<BookDto> findAll() {
		List<Book> books = bookRepository.findAll();
		List<BookDto> booksDto = bookMapper.fromDomainListToDtoList(books);
		return booksDto;
	}

	@Override
	public BookDto findById(Integer id) {
		Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);
		BookDto bookDto = bookMapper.fromDomainToDto(book);
		return bookDto;
	}

	@Override
	public List<BookDto> findByBookName(String bookName) {
		List<Book> books = bookRepository.findByBookName(bookName);
		List<BookDto> booksDto = bookMapper.fromDomainListToDtoList(books);
		return booksDto;
	}

	@Override
	public void saveBook(BookDto bookDto) {
		Book book = bookMapper.fromDtoToDomain(bookDto);
		bookRepository.save(book);
	}

	@Override
	public void deleteById(Integer id) {
		bookRepository.deleteById(id);
	}

	@Override
	public void update(Integer id, BookDto bookDto) {
		Book book = bookMapper.fromDtoToDomain(bookDto);
		bookRepository.save(book);
	}

}
