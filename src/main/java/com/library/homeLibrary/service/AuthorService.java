package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.library.homeLibrary.dto.AuthorDto;

@Service
public interface AuthorService {

	public AuthorDto findById(Integer id);

	public void saveAuthor(AuthorDto authorDto);

	public List<AuthorDto> findAll();

	public void deleteById(Integer id);

	public void update(Integer id, AuthorDto authorDto);
}
