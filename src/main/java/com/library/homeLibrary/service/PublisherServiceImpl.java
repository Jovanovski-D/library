package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.homeLibrary.domain.Publisher;
import com.library.homeLibrary.dto.PublisherDto;
import com.library.homeLibrary.mappers.PublisherMapper;
import com.library.homeLibrary.repository.PublisherRepository;

@Service
public class PublisherServiceImpl implements PublisherService {

	@Autowired
	private PublisherRepository publisherRepository;
	@Autowired
	private PublisherMapper publisherMapper;

	@Override
	public List<PublisherDto> findAll() {
		List<Publisher> publishers = publisherRepository.findAll();
		List<PublisherDto> publishersDto = publisherMapper.fromDomainListToDtoList(publishers);
		return publishersDto;
	}

	@Override
	public PublisherDto findById(Integer id) {
		Publisher publisher = publisherRepository.findById(id).orElseThrow(RuntimeException::new);
		PublisherDto publisherDto = publisherMapper.fromDomainToDto(publisher);
		return publisherDto;
	}

	@Override
	public void savePublisher(PublisherDto publisherDto) {
		Publisher publisher = publisherMapper.fromDtoToDomain(publisherDto);
		publisherRepository.save(publisher);
	}

	@Override
	public void deleteById(Integer id) {
		publisherRepository.deleteById(id);
	}

	@Override
	public void update(Integer id, PublisherDto publisherDto) {
		Publisher publisher = publisherMapper.fromDtoToDomain(publisherDto);
		publisherRepository.save(publisher);

	}

}
