package com.library.homeLibrary.service;

import java.util.List;

import com.library.homeLibrary.dto.BookDto;

public interface BookService {

	public BookDto findById(Integer id);

	public void saveBook(BookDto bookDto);

	public List<BookDto> findAll();

	public List<BookDto> findByBookName(String bookName);

	public void deleteById(Integer id);

	public void update(Integer id, BookDto bookDto);
}
