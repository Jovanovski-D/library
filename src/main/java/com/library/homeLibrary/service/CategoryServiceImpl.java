package com.library.homeLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.homeLibrary.domain.Category;
import com.library.homeLibrary.dto.CategoryDto;
import com.library.homeLibrary.mappers.CategoryMapper;
import com.library.homeLibrary.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private CategoryMapper categoryMapper;

	@Override
	public List<CategoryDto> findAll() {
		List<Category> categories = categoryRepository.findAll();
		List<CategoryDto> categoriesDto = categoryMapper.fromDomainListToDtoList(categories);
		return categoriesDto;
	}

	@Override
	public CategoryDto findById(Integer id) {
		Category category = categoryRepository.findById(id).orElseThrow(RuntimeException::new);
		CategoryDto categoryDto = categoryMapper.fromDomainToDto(category);
		return categoryDto;
	}

	@Override
	public void saveCategory(CategoryDto categoryDto) {
		Category category = categoryMapper.fromDtoToDomain(categoryDto);
		categoryRepository.save(category);
	}

	@Override
	public void deleteById(Integer id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public void update(Integer id, CategoryDto categoryDto) {
		Category category = categoryMapper.fromDtoToDomain(categoryDto);
		categoryRepository.save(category);
	}

}
