package com.library.homeLibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.library.homeLibrary.dto.PublisherDto;
import com.library.homeLibrary.service.PublisherService;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("publisher")
public class PublisherController {
	
	@Autowired
	private PublisherService publisherService;

	@GetMapping
	public List<PublisherDto> findAll() {
		return publisherService.findAll();
	}

	@GetMapping("/{id}")
	public PublisherDto findById(@PathVariable Integer id) {
		return publisherService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public void create(@RequestBody PublisherDto publisherDto) {
		publisherService.savePublisher(publisherDto);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@PathVariable(value = "id") Integer id, @RequestBody PublisherDto publisherDto) {
		publisherDto.setId(id);
		publisherService.update(id, publisherDto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Integer id) {
		publisherService.deleteById(id);
	}

}
