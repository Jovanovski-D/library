package com.library.homeLibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.library.homeLibrary.dto.AuthorDto;
import com.library.homeLibrary.service.AuthorServiceImpl;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("author")
public class AuthorController {

	@Autowired
	private AuthorServiceImpl authorService;

	@GetMapping
	public List<AuthorDto> findAll() {
		return authorService.findAll();
	}

	@GetMapping("/{id}")
	public AuthorDto findById(@PathVariable Integer id) {
		return authorService.findById(id);
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public void create(@RequestBody AuthorDto authorDto) {
		authorService.saveAuthor(authorDto);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@PathVariable(value = "id") Integer id, @RequestBody AuthorDto authorDto) {
		authorDto.setId(id);
		authorService.update(id, authorDto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Integer id) {
		authorService.deleteById(id);
	}
}
