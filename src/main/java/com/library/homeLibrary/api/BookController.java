package com.library.homeLibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.library.homeLibrary.dto.BookDto;
import com.library.homeLibrary.service.BookService;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("book")
public class BookController {
	
	@Autowired
	private BookService bookService;

	@GetMapping
	public List<BookDto> findAll() {
		return bookService.findAll();
	}

	@GetMapping("/{id}")
	public BookDto findById(@PathVariable Integer id) {
		return bookService.findById(id);
	}
	
	@GetMapping("/{bookName}")
	public List<BookDto> findByBookName(@PathVariable String bookName) {
		return bookService.findByBookName(bookName);
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public void create(@RequestBody BookDto bookDto) {
		bookService.saveBook(bookDto);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@PathVariable(value = "id") Integer id, @RequestBody BookDto bookDto) {
		bookDto.setId(id);
		bookService.update(id, bookDto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Integer id) {
		bookService.deleteById(id);
	}

}
