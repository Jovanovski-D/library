package com.library.homeLibrary.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "publishers")
public class Publisher {

	@Id
//	@SequenceGenerator(name = "publisher_seq", sequenceName = "publisher_seq1")
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "publisher_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "publisher_name", length = 50, nullable = false)
	private String publisherName;

	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Book> books;
}
