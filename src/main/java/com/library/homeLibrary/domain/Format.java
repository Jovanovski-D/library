package com.library.homeLibrary.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "formats")
public class Format {

	@Id
//	@SequenceGenerator(name = "format_seq", sequenceName = "format_seq1")
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "format_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "format_name", length = 50, nullable = false)
	private String formatName;

	@ManyToMany(mappedBy = "formats", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Book> books;

}
