package com.library.homeLibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.library.homeLibrary.domain.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

}
