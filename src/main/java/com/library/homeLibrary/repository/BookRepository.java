package com.library.homeLibrary.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.library.homeLibrary.domain.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

	public List<Book> findByBookName(String name); //TODO najdi kaj se koristi , smeni go od Book vo List<Books>
}
