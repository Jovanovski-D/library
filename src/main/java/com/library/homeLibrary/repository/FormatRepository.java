package com.library.homeLibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.library.homeLibrary.domain.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Integer> {

}
