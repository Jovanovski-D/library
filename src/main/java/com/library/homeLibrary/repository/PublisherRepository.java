package com.library.homeLibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.library.homeLibrary.domain.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer>{

}
