import React from 'react';
import BookService from './BookService';
import axios from 'axios';
import './components.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class BookComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            books: [],
            foundBook: '',
            enteredId: null,
            enteredBookName: null
        }
        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangeBookName = this.handleChangeBookName.bind(this);
    }
    handleChangeId(event) {
        this.setState({ enteredId: event.target.value });
    }

    handleChangeBookName(event) {
        this.setState({ enteredBookName: event.target.value });
    }


    findBookById = () => {
        let url = 'http://localhost:8080/book/' + this.state.enteredId;
        axios.get(url).then((response) => {
            this.setState({ foundBook: response.data })
        });
    }
    saveBook = () => {
        let url = 'http://localhost:8080/book/';
        let book = {
            bookName: this.state.enteredBookName
        }
        axios.post(url, book);
    }
    updateBook = () => {
        let url = 'http://localhost:8080/book/' + this.state.enteredId;;
        let book = {
            bookName: this.state.enteredBookName
        }
        axios.put(url, book);
    }
    deleteBookById = () => {
        let url = 'http://localhost:8080/book/' + this.state.enteredId;
        axios.delete(url);
    }

    componentDidMount(){
        BookService.getBooks().then((response) => {
            this.setState({ books: response.data})
        });
    }

    render (){
        return (
            <div>
                <h2 className = "text-center">All Books</h2>
                <table className = "table">
                    <thead>
                        <tr>
                            <td> Book Id</td>
                            <td> Book Name</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.books.map(
                                book => 
                                <tr key = {book.id}>
                                     <td> {book.id}</td>   
                                     <td> {book.bookName}</td>   
                                </tr>
                            )
                        }

                    </tbody>
                </table>

                <form onSubmit={BookService.handleSubmit}>
                    <br />
                    <label> Book Id  </label>
                    <br />
                    <input type="number" name="enteredId" placeholder="Book Id" value={this.state.enteredId} onChange={this.handleChangeId} />
                    <br />
                    <button type="button" onClick={this.deleteBookById}>Delete Book by Id</button>
                    <button type="button" onClick={this.findBookById}>Find Book By id</button>
                    <br /><br />

                    <label>Book Name </label> <br />
                    <input type="text" name="enteredBookName" placeholder="Book Name" value={this.state.enteredBookName} onChange={this.handleChangeBookName} />
                </form>

                <button type="button" onClick={this.saveBook}>Save Book</button>
                <button type="button" onClick={this.updateBook}>Update Book</button>
                <br />
                <h2 className="text-center">Found Book</h2>
                <label>
                    Book Id: {this.state.foundBook.id}
                </label><br />
                <label>
                    Book Name: {this.state.foundBook.bookName}
                </label>

            </div>

        )
    }
}


export default BookComponent;