import React from 'react';
import CategoryService from './CategoryService';
import axios from 'axios';
import './components.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class CategoryComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            categories: [],
            foundCategory: '',
            enteredId: null,
            enteredCategoryName: null
        }
        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangeCategoryName = this.handleChangeCategoryName.bind(this);
    }
    handleChangeId(event) {
        this.setState({ enteredId: event.target.value });
    }

    handleChangeCategoryName(event) {
        this.setState({ enteredCategoryName: event.target.value });
    }


    findCategoryById = () => {
        let url = 'http://localhost:8080/category/' + this.state.enteredId;
        axios.get(url).then((response) => {
            this.setState({ foundCategory: response.data })
        });
    }
    saveCategory = () => {
        let url = 'http://localhost:8080/category/';
        let category = {
            categoryName: this.state.enteredCategoryName
        }
        axios.post(url, category);
    }
    updateCategory = () => {
        let url = 'http://localhost:8080/category/' + this.state.enteredId;;
        let category = {
            categoryName: this.state.enteredCategoryName
        }
        axios.put(url, category);
    }
    deleteCategoryById = () => {
        let url = 'http://localhost:8080/category/' + this.state.enteredId;
        axios.delete(url);
    }

    componentDidMount() {
        CategoryService.getCategories().then((response) => {
            this.setState({ categories: response.data })
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">All Categories</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <td> Category Id</td>
                            <td> Category Name</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.categories.map(
                                category =>
                                    <tr key={category.id}>
                                        <td> {category.id}</td>
                                        <td> {category.categoryName}</td>
                                    </tr>
                            )
                        }

                    </tbody>
                </table>

                <form onSubmit={CategoryService.handleSubmit}>
                    <br />
                    <label> Category Id  </label>
                    <br />
                    <input type="number" name="enteredId" placeholder="Category Id" value={this.state.enteredId} onChange={this.handleChangeId} />
                    <br />
                    <button type="button" onClick={this.deleteCategoryById}>Delete Category by Id</button>
                    <button type="button" onClick={this.findCategoryById}>Find Category By id</button>
                    <br /><br />

                    <label>Category Name </label> <br />
                    <input type="text" name="enteredCategoryName" placeholder="Category Name" value={this.state.enteredCategoryName} onChange={this.handleChangeCategoryName} />
                </form>

                <button type="button" onClick={this.saveCategory}>Save Category</button>
                <button type="button" onClick={this.updateCategory}>Update Category</button>
                <br />
                <h2 className="text-center">Found Category</h2>
                <label>
                    Category Id: {this.state.foundCategory.id}
                </label><br />
                <label>
                    Category Name: {this.state.foundCategory.categoryName}
                </label>

            </div>

        )
    }
}


export default CategoryComponent;