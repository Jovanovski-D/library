import axios from 'axios'

const FORMATS_REST_API_URL = 'http://localhost:8080/format';

class FormatService {

    getFormats() {
        return axios.get(FORMATS_REST_API_URL);
    }
}

export default new FormatService();