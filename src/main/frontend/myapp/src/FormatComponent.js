import React from 'react';
import FormatService from './FormatService';
import axios from 'axios';
import './components.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class FormatComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            formats: [],
            foundFormat: '',
            enteredId: null,
            enteredFormatName: null
        }
        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangeFormatName = this.handleChangeFormatName.bind(this);
    }
    handleChangeId(event) {
        this.setState({ enteredId: event.target.value });
    }

    handleChangeFormatName(event) {
        this.setState({ enteredFormatName: event.target.value });
    }


    findFormatById = () => {
        let url = 'http://localhost:8080/format/' + this.state.enteredId;
        axios.get(url).then((response) => {
            this.setState({ foundFormat: response.data })
        });
    }
    saveFormat = () => {
        let url = 'http://localhost:8080/format/';
        let format = {
            formatName: this.state.enteredFormatName
        }
        axios.post(url, format);
    }
    updateFormat = () => {
        let url = 'http://localhost:8080/format/' + this.state.enteredId;;
        let format = {
            formatName: this.state.enteredFormatName
        }
        axios.put(url, format);
    }
    deleteFormatById = () => {
        let url = 'http://localhost:8080/format/' + this.state.enteredId;
        axios.delete(url);
    }

    componentDidMount() {
        FormatService.getFormats().then((response) => {
            this.setState({ formats: response.data })
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">All Formats</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <td> Format Id</td>
                            <td> Format Name</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.formats.map(
                                format =>
                                    <tr key={format.id}>
                                        <td> {format.id}</td>
                                        <td> {format.formatName}</td>
                                    </tr>
                            )
                        }

                    </tbody>
                </table>

                <form onSubmit={FormatService.handleSubmit}>
                    <br />
                    <label> Format Id  </label>
                    <br />
                    <input type="number" name="enteredId" placeholder="Format Id" value={this.state.enteredId} onChange={this.handleChangeId} />
                    <br />
                    <button type="button" onClick={this.deleteFormatById}>Delete Format by Id</button>
                    <button type="button" onClick={this.findFormatById}>Find Format By id</button>
                    <br /><br />

                    <label>Format Name </label> <br />
                    <input type="text" name="enteredFormatName" placeholder="Format Name" value={this.state.enteredFormatName} onChange={this.handleChangeFormatName} />
                </form>

                <button type="button" onClick={this.saveFormat}>Save Format</button>
                <button type="button" onClick={this.updateFormat}>Update Format</button>
                <br />
                <h2 className="text-center">Found Format</h2>
                <label>
                    Format Id: {this.state.foundFormat.id}
                </label><br />
                <label>
                    Format Name: {this.state.foundFormat.formatName}
                </label>

            </div>

        )
    }
}


export default FormatComponent;