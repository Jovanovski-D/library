import React, { Component } from 'react';
import './App.css';
import axios from "axios";
import AuthorComponent from './AuthorComponent';
import PublisherComponent from './PublisherComponent';
import CategoryComponent from './CategoryComponent';
import FormatComponent from './FormatComponent';
import BookComponent from './BookComponent';
import ButtonRow from './ButtonRow';

class App extends Component {
  state = {
    authors: null,
    visible: true,
    whichComponentToShow: null
  }
  getAuthors = () => {
    this.setState({ whichComponentToShow: "AuthorComponent" });
  }
  getPublishers = () => {
    this.setState({ whichComponentToShow: "PublisherComponent" });
  }
  getFormats = () => {
    this.setState({ whichComponentToShow: "FormatComponent" });
  }
  getCategories = () => {
    this.setState({ whichComponentToShow: "CategoryComponent" });
  }
  getBooks = () => {
    this.setState({ whichComponentToShow: "BookComponent" });
  }
  getAuthorById = (e) => {
    e.preventDefault();
    const author = e.target.elements.authorid.value;
    if (author) {
      axios.get(`http://localhost:8080/author/${author}`)
        .then((res) => {
          const resultAuthor = res.data.author;
          this.setState({ authors: resultAuthor });
        })
    } else return;
  }

  render() {
    if (this.state.whichComponentToShow === "AuthorComponent") {
      return (
        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
          <AuthorComponent />
        </div>
      )

    } else if (this.state.whichComponentToShow === "PublisherComponent") {
      return (
        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
          <PublisherComponent />
        </div>
      )
    } else if (this.state.whichComponentToShow === "CategoryComponent") {
      return (
        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
          <CategoryComponent />
        </div>
      )
    } else if (this.state.whichComponentToShow === "FormatComponent") {
      return (
        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
          <FormatComponent />
        </div>
      )
    } else if (this.state.whichComponentToShow === "BookComponent") {
      return (
        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
          <BookComponent />
        </div>
      )
    } else {
      return (

        <div class="App">
          <div class="navbar">
            <button type="button" onClick={this.getBooks}>Books</button>
            <button type="button" onClick={this.getAuthors}>Authors</button>
            <button type="button" onClick={this.getPublishers}>Publishers</button>
            <button type="button" onClick={this.getFormats}>Formats</button>
            <button type="button" onClick={this.getCategories}>Categories</button>
          </div>
        </div>

      )
    }
  }
}

export default App;
