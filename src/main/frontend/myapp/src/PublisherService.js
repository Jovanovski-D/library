import axios from 'axios'

const PUBLISHER_REST_API_URL = 'http://localhost:8080/publisher';

class PublisherService {

    getPublishers(){
        return axios.get(PUBLISHER_REST_API_URL);
    }
}

export default new PublisherService();