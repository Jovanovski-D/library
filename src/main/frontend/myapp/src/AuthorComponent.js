import React from 'react';
import AuthorService from './AuthorService';
import axios from 'axios';
import './components.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class AuthorComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            authors: [],
            foundAuthor: '',
            enteredId: null,
            enteredFirstName: null
        }
        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
    }
    handleChangeId(event) {
        this.setState({ enteredId: event.target.value });
    }

    handleChangeFirstName(event) {
        this.setState({ enteredFirstName: event.target.value });
    }


    findAuthorById = () => {
        let url = 'http://localhost:8080/author/' + this.state.enteredId;
        axios.get(url).then((response) => {
            this.setState({ foundAuthor: response.data })
        });
    }
    saveAuthor = () => {
        let url = 'http://localhost:8080/author/';
        let author = {
            firstName: this.state.enteredFirstName
        }
        axios.post(url, author);
    }
    updateAuthor = () => {
        let url = 'http://localhost:8080/author/' + this.state.enteredId;;
        let author = {
            firstName: this.state.enteredFirstName
        }
        axios.put(url, author);
    }
    deleteAuthorById = () => {
        let url = 'http://localhost:8080/author/' + this.state.enteredId;
        axios.delete(url);
    }


    componentDidMount() {
        AuthorService.getAuthors().then((response) => {
            this.setState({ authors: response.data })
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">All Authors</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <td> Author Id</td>
                            <td> Author First Name</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.authors.map(
                                author =>
                                    <tr key={author.id}>
                                        <td> {author.id}</td>
                                        <td> {author.firstName}</td>
                                    </tr>
                            )
                        }

                    </tbody>
                </table>

                <form onSubmit={AuthorService.handleSubmit}>
                    <br />
                    <label> Author Id  </label>
                    <br />
                    <input type="number" name="enteredId" placeholder="Author Id" value={this.state.enteredId} onChange={this.handleChangeId} />
                    <br />
                    <button type="button" onClick={this.deleteAuthorById}>Delete Author by Id</button>
                    <button type="button" onClick={this.findAuthorById}>Find Author By id</button>
                    <br /><br />

                    <label>Author First Name </label> <br />
                    <input type="text" name="enteredFirstName" placeholder="Author First Name" value={this.state.enteredFirstName} onChange={this.handleChangeFirstName} />
                </form>

                <button type="button" onClick={this.saveAuthor}>Save Author</button>
                <button type="button" onClick={this.updateAuthor}>Update Author</button>
                <br />
                <h2 className="text-center">Found Author</h2>
                <label>
                    Author Id: {this.state.foundAuthor.id}
                </label><br />
                <label>
                    Author First Name: {this.state.foundAuthor.firstName}
                </label>
            </div>


        )
    }
}


export default AuthorComponent;