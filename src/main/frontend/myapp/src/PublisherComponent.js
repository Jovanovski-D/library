import React from 'react';
import PublisherService from './PublisherService';
import axios from 'axios';
import './components.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class PublisherComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            publishers: [],
            foundPublisher: '',
            enteredId: null,
            enteredPublisherName: null
        }
        this.handleChangeId = this.handleChangeId.bind(this);
        this.handleChangePublisherName = this.handleChangePublisherName.bind(this);
    }
    handleChangeId(event) {
        this.setState({ enteredId: event.target.value });
    }

    handleChangePublisherName(event) {
        this.setState({ enteredPublisherName: event.target.value });
    }

    findPublisherById = () => {
        let url = 'http://localhost:8080/publisher/' + this.state.enteredId;
        axios.get(url).then((response) => {
            this.setState({ foundPublisher: response.data })
        });
    }
    savePublisher = () => {
        let url = 'http://localhost:8080/publisher/';
        let publisher = {
            publisherName: this.state.enteredPublisherName
        }
        axios.post(url, publisher);
    }
    updatePublisher = () => {
        let url = 'http://localhost:8080/publisher/' + this.state.enteredId;;
        let publisher = {
            publisherName: this.state.enteredPublisherName
        }
        axios.put(url, publisher);
    }
    deletePublisherById = () => {
        let url = 'http://localhost:8080/publisher/' + this.state.enteredId;
        axios.delete(url);
    }

    componentDidMount() {
        PublisherService.getPublishers().then((response) => {
            console.log(response.data);
            this.setState({ publishers: response.data })
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">All Publishers</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <td> Publisher Id</td>
                            <td> Publisher Name</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.publishers.map(
                                publisher =>
                                    <tr key={publisher.id}>
                                        <td> {publisher.id}</td>
                                        <td> {publisher.publisherName}</td>
                                    </tr>
                            )
                        }

                    </tbody>
                </table>

                <form onSubmit={PublisherService.handleSubmit}>
                    <br />
                    <label> Publisher Id  </label>
                    <br />
                    <input type="number" name="enteredId" placeholder="Publisher Id" value={this.state.enteredId} onChange={this.handleChangeId} />
                    <br />
                    <button type="button" onClick={this.deletePublisherById}>Delete Publisher by Id</button>
                    <button type="button" onClick={this.findPublisherById}>Find Publisher By id</button>
                    <br /><br />

                    <label>Publisher Name </label> <br />
                    <input type="text" name="enteredPublisherName" placeholder="Publisher Name" value={this.state.enteredPublisherName} onChange={this.handleChangePublisherName} />
                </form>

                <button type="button" onClick={this.savePublisher}>Save Publisher</button>
                <button type="button" onClick={this.savePublisher}>Update Publisher</button>
                <br />
                <h2 className="text-center">Found Publisher</h2>
                <label>
                    Publisher Id: {this.state.foundPublisher.id}
                </label><br />
                <label>
                    Publisher Name: {this.state.foundPublisher.publisherName}
                </label>

            </div>

        )
    }


}


export default PublisherComponent;