package com.library.homeLibrary.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.library.homeLibrary.domain.Author;
import com.library.homeLibrary.dto.AuthorDto;
import com.library.homeLibrary.mappers.AuthorMapper;
import com.library.homeLibrary.repository.AuthorRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorServiceTest {

	private AuthorService authorService;
	@Mock
	private AuthorRepository authorRepository;
	@Mock
	private AuthorMapper authorMapper;
	@Mock
	private Author author;
	@Mock
	private AuthorDto authorDto;
	
// 	@Test
// 	public void shouldFindById() {
// //		//given
// 		authorService = new AuthorServiceImpl();
// 		when(authorRepository.findById(anyInt())).thenReturn(author);
// 		when(authorMapper.fromDomainToDto(any())).thenReturn(authorDto);
// 		when(author.getId()).thenReturn(1);
// //		//when
// 		AuthorDto resultDto = authorService.findById(1);
// //		//then
// 		assertEquals(resultDto.getId(), authorDto.getId());
// 		assertEquals(resultDto.getFirstName(), authorDto.getFirstName());
// 	}

	@Test
	public void shouldDeleteById() {
		//given
		authorService = new AuthorServiceImpl();
		//when
		authorService.deleteById(1);
		//then
		ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
		verify(authorRepository).deleteById(captor.capture());
		assertEquals(1, captor.getValue());
	}


	@Test
	public void shouldSaveAuthor() {
		//given
		authorService = new AuthorServiceImpl();
		when(authorMapper.fromDtoToDomain(any())).thenReturn(author);
		//when
		authorService.saveAuthor(authorDto);
		//then
		ArgumentCaptor<Author> captor = ArgumentCaptor.forClass(Author.class);
		verify(authorRepository).save(captor.capture());

		assertEquals(authorDto.getId(), captor.getValue().getId());
		assertEquals(authorDto.getFirstName(), captor.getValue().getFirstName());
	}
}